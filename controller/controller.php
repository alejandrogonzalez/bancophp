<?php

require_once('../model/Cliente.php');
require_once('../model/ClienteModel.php');

use Cliente;

require_once('../helper/validations.php');

if ( isset($_POST['submit'])) {




    if ( $_POST['control'] == 'register') {
        $_POST['message'] = validateRegister();

        if ($_POST['message'] == '') {
            
            header('Location: ../views/login.php');

        } else {
            require_once('../views/register.php');
        }

    }

    if ( $_POST['control'] == 'login') {
        $hash = getUserHash($_POST['dni']);
        if (password_verify($_POST['password'],$hash)) {
            session_start();
            $_SESSION['user'] = $_POST['dni'];
            header('Location: ../views/welcome.php');
        }else{
            require_once('..views/login.php');
        }

    }

}

?>