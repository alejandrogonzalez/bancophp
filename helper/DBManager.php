<?php

require_once ('../config/config.php');

class DBManager extends PDO{
    public $server = SERVER;
    public $user = USER;
    public $pass = PASS;
    public $port = PORT;
    public $db = DB;
    private $conexion = CONEXION;

    public function __construct(){
        $this -> conectar();
    }

    private final function conectar(){
        $conexion = null;

        try{
            if(is_array(PDO::getAvailableDrivers())){
                if(in_array('pgsql',PDO::getAvailableDrivers())){
                    $conexion = new PDO("$this->server,$this->port,$this->db,$this->user,$this->pass");
                }else{
                    throw new PDOException("ERROR");
                }
            }
        }catch (PDOException $e){
            echo $e -> getMessage();
        }
        $this->setConexion($conexion);
    }
    public final function setConexion($conexion){
        $this->conexion = $conexion;
    }

    public final function getConexion(){
        return $this->conexion;
    }

    public final function cerrarConexion(){
        $this->conexion(null);
    }

}


?>
