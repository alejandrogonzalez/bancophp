<?php
require_once('../db/DBManager.php');
use DBManager;

function insertCliente($cliente){

    $manager = new DBManager();

    try{
        $sql = "INSERT INTO cliente(nombre,apellidos,fecha,genero,telephone,dni,email,password) 
        VALUES (:nombre,:apelliods,:fecha,:genero,:telephone,:dni,:email,:password)";

        $password = password_hash($cliente -> getPassword(),PASSWORD_DEFAULT,['cost' => 10]);

        $stmt = $manager -> getConexion() -> prepare($sql);
        $stmt -> bindParam(':nombre',$cliente -> getNombre());
        $stmt -> bindParam(':apellidos',$cliente -> getApellidos());
        $stmt -> bindParam(':fecha',$cliente -> getFecha());
        $stmt -> bindParam(':genero',$cliente -> getGenero());
        $stmt -> bindParam(':telephone',$cliente -> getTelefono());
        $stmt -> bindParam(':dni',$cliente -> getDni());
        $stmt -> bindParam(':email',$cliente -> getEmail());
        $stmt -> bindParam(':password',$password);

        if($stmt -> execute()){
            echo "todo OK";
        }else{
            echo "MAL";
        }

    }catch (PDOException $e){
        echo $e -> getMessage();
    }
}


function getUserHash($dni){
    $conexion = new DBManager();
    try{
        $sql = "SELECT * FROM cliente WHERE dni =: dni";
        $stmt = $conexion -> getConexion() -> prepare($sql);
        $stmt -> bindParam(':dni', $dni);
        $stmt -> execute();
        $result = $stmt -> fetchAll(PDO::FETCH_ASSOC);
        return $result[0]['password'];
    }catch (PDOException $e){
        echo $e -> getMessage();
    }
}




?>